@echo off
Setlocal

REM -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
REM File:			unlock.bat
REM Version:		V1.3
REM Author:			Kamaradski 2014
REM Contributers:		none
REM
REM Arma3 file unlocker script written for Ahoyworld.co.uk
REM
REM -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-



REM =-=-=-=-=-=-=-=-=-=-=-=-=-=- CONFIGURATION -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
REM PATHHANDLE = Your path to handle.exe, leave empty if it can be found in the system %PATH%
REM EXTLOOP = The file-extension of the files you like to unblock (without * or .)

set PATHHANDLE=C:\apps\Handle
set EXTLOOP=pbo

REM =-=-=-=-=-=-=-=-=-=-=-=-=- DO NOT EDIT BELOW HERE -=-=-=-=-=-=-=-=-=-=-=-=-=-

for /f "eol=: delims=" %%F in ('dir /b /o-d "%~dp0\*.%EXTLOOP%"') do (
cls
echo.
echo =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
echo            AhoyWorld.co.uk - File Unlocker by Kamaradski
echo =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
echo.
echo Unlocking file: 
echo.
echo "%~dp0\%%F"
echo.
@ping 127.0.0.1 -n 2 -w 1000 > nul


	for /F "tokens=3,6 delims=: " %%I IN ('"%PATHHANDLE%\handle.exe %%F"') DO "%PATHHANDLE%\handle.exe" -c %%J -y -p %%I
	)
)
exit