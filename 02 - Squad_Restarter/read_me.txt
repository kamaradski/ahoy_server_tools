// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// File:			SQ_Restarter.ps1
// Author:			Kamaradski
//
// //Ahoyworld.co.uk - Squad restarter by Kamaradski
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-



What:
This script will start & re-start your Squad server and performs maintenance


Documentation:
https://docs.google.com/document/d/12BuIfNB8puUpwy2OAGsJ2KApqK-DnciQNFnwSFA_t7o


Main features:
- Start & re-start Squad Server
- Allows you to set the CPU-core()s) the Server is running on
- Update and validate your Squad Server installation via SteamCMD
- Automatic Server Configuration & Log backup


Usage Licence & Warranty disclaimer:
See Documentation - page-2


Contribution:
In case you like to improve this script: Send us your code suggestions on bitbucket or email


Donations:
If you like this script, please support us by donating something to our paypal:
https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=WHH8VTC58XU7G