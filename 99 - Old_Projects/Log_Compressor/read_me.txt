// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// File:			Compressor.bat
// Author:			Kamaradski 2014
// Contributers:		none
//
// Arma3 Log Compressor script written for Ahoyworld.co.uk
//
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-


What:
This script loops through all files with a sepcific extension, in the folder where it is located, 
and packs all files except the xx latest files into a zip file with the same name. 
It deletes the original file



How:
1 - Edit the file extension the script is running for (default: *.log)
2 - Edit the Path to where 7z.exe is located (Default: c:\apps\7-Zip)
3 - Edit the number of files you like not to compress and leave untouched in plain text
4 - Copy the script in to the directory where you want it to run
5 - Manually run the script by double-clicking it, or start it from another script (example: backup script)


Dependancies:
This script requires thirdparty software called: 7z.exe (7-Zip) from Igor Pavlov.
http://www.7-zip.org/


The user can:
- Configure the path to 7z.exe
- Configure what file-extension the script will run against
- Configure how many of the latest files should remain untouched in plain text


What does Ahoyworld use it for ?
- We call from our backup script, insuring twice a week that the old logs are compressed and backupped
- We have this script located in all log folders for our Arma, Minecraft and Teamspeak servers, incl. BEC


Contribution:
In case you like to improve this script: Please FORK it on Bitbucket Git, and send us a pull-request.