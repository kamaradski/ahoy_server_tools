// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// File:			A3_Server_updater.bat
// Author:			Kamaradski 2014
// Contributers:		Unknown
//
// Arma3 Server updater script written for Ahoyworld.co.uk
//
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-


What:
This script will update your Arma3 Servers via SteamCMD, it will keep a clean 
master copy on your hardrive and then updates your live servers by autodetection.
It will also clean your live servers from clutter, deleted files and userdata left behind.


How:
- Download SteamCMD to your server harddrive and move it to the folder where you want it to live
- Extract and delete the .zip container it came in
- Start the SteamCMD.exe for the first time, it will download and update itself
- Exit the SteamCMD console by typing 'exit' + [enter]
- Store this Server_Updater script anywhere on your server
- Edit the script to tell it where it can find SteamCMD
- Edit the script to tell it where you want your permanent master-copy of the clean arma server
		(This is the mastercopy that actually will be updated by the script, and copied to your live servers)
- Edit the script to tell it the root of your Arma3 servers. 
		(example C:\Games) The script will scan all subfolders searching for arma3server.exe
- If you use this script to make your initial installation, you will have to make sure it can find 
	arma3server.exe in the subfolders where you want your arma3 servers to be. You can fool the script 
	by creating such file yourself, for exmple to rename an empty text-file.
- Run the script, indicate the branch you want to update, fil-in your steam details, 
	Wait for the master-copy to be updated, select if you want to update only a specific server, or all of them at once


Dependancies:
This script depends on thirdparty software called SteamCMD (Steam commandline interface)
https://developer.valvesoftware.com/wiki/SteamCMD


The user can:
- Select where SteamCMD lives
- Select where the master-copy will be stored
- Select the root of where the arma3 servers are installed (the script will detect the servers in the sub-folders automatically)
- Select to what branch will be updated (Stable\Development)
- Login with any steam account upon update (credentials are not stored)
- Decide to update only one server or all at once


What does Ahoyworld use it for ?
- We use it to update our I&A servers, and purge cache & user data clutter upon each update


Contributors:
The actual part that deals with steamCMD was mostly written by an unknown source
If anyone knows the source of this code, please inform me (kamaradski) so i can add the credits


Contribution:
In case you like to improve this script: Please FORK it on Bitbucket Git, and send us a pull-request