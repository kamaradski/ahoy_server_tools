// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// File:			Reboot.bat
// Author:			Kamaradski 2014
// Contributers:	none
//
// Dedicated server reboot script written for Ahoyworld.co.uk
//
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-


What:
This script simply restarts all your services after a windows reboot
It has a startup delay in order not to start everything simultaniously



How:
1 - Edit the full paths to the executables, batch files, or links you want to start
2 - Add or remove services at will


Dependancies:
none


What does Ahoyworld use it for ?
- We use it to restart all our services we have running after a reboot, so we do not have 
	to login to RDP after we initiated the reboot


Contribution:
In case you like to improve this script: Please FORK it on Bitbucket Git, and send us a pull-request.