@echo off
Setlocal
REM -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
REM File:			Compressor.bat
REM Version:		V1.3
REM Author:			Kamaradski 2014
REM Contributers:	none
REM
REM
REM Arma3 Log Compressor script written for Ahoyworld.co.uk
REM
REM -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-



REM =-=-=-=-=-=-=-=-=-=-=-=-=-=- CONFIGURATION -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
REM PATHHANDLE = Your path to 7-Zip, leave empty if it can be found in the system %PATH%
REM EXTLOOP = The file-extension of the files you like to compress (without * or .)
REM NUMKEEP = The number of latest files you wish to keep untouched (plain text)
REM ArchiveName = Name of the archive file that will be created (excluding extension)

set PATHZIP=c:\apps\7-Zip
set EXTLOOP=log
set NUMKEEP=5
SET ArchiveName=Log-archive

REM =-=-=-=-=-=-=-=-=-=-=-=-=- DO NOT EDIT BELOW HERE -=-=-=-=-=-=-=-=-=-=-=-=-=-

set /a cnt=0
set /a cntr=0
set "keep=%NUMKEEP%"
for /f "eol=: delims=" %%F in ('dir /b /o-d "%~dp0\*.%EXTLOOP%"') do (
set /a cntr=cntr+1
cls
echo.
echo =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
echo            AhoyWorld.co.uk - Log compressor by Kamaradski
echo =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
echo.
echo Processing log-file: 
echo.
echo "%~dp0\%%F"
echo.
ping 127.0.0.1 -n 1 -w 1000 > nul

  if defined keep (
    2>nul set /a "cnt+=1, 1/(keep-cnt)" || set "keep="
  ) else ( 
	"%PATHZIP%\7z.exe" a -tzip -mx=9 "%~dp0\%ArchiveName%.zip" "%~dp0\%%F"
	del "%~dp0\%%F"
	)
)

cls
echo.
echo =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
echo            AhoyWorld.co.uk - Log compressor by Kamaradski
echo =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
echo.
echo Added %cntr% file(s) to:
echo.
echo "%~dp0\%ArchiveName%.zip"
echo.
echo.
ping 127.0.0.1 -n 4 -w 1000 > nul

exit