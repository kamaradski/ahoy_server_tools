@echo off
SETLOCAL

REM -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
REM File:			Backup_Tool.bat
REM Version:		V1.2
REM Author:			Kamaradski 2014
REM Contributers:	none
REM
REM Dedicated Server Backup script written for Ahoyworld.co.uk
REM
REM -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-


REM =-=-=-=-=-=-=-=-=-=-=-=-=-=- CONFIGURATION -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
REM Variables containing TARGET = the PATH inside the Backupmaster locacation

set LOGCOMPRESSION=YES
set INTERVAL=5

set Backupmaster=C:\Sync-folders\Backup

set EU1=C:\Games\A3_EU1
set EU1TARGET=Games\A3_EU1

set EU2=C:\Games\A3_EU2
set EU2TARGET=Games\A3_EU2

set EU3=C:\Games\A3_EU3_GameNight
set EU3TARGET=Games\A3_EU3_GameNight

set TOOL=C:\Games\Tools
set TOOLTARGET=Games\Tools

set APPS=C:\apps
set APPSTARGET=apps

set MCV=C:\Games\Minecraft
set MCVTARGET=Games\Minecraft

set MCM=C:\Games\Minecraft - Modded
set MCMTARGET=Games\Minecraft - Modded

REM =-=-=-=-=-=-=-=-=-=-=-=-=- DO NOT EDIT BELOW HERE -=-=-=-=-=-=-=-=-=-=-=-=-=-


cls
for /L %%A in (%INTERVAL%,-1,0) do ( 
echo.
echo =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
echo            AhoyWorld.co.uk - Server Backup by Kamaradski
echo =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
echo.
echo Starting backup "Arma3 EU#1" in %%A seconds.
echo.
ping localhost -n 2 >nul 
cls 
)
if /i %LOGCOMPRESSION% == YES (
	cd "%EU1%"
	for /r %%X in (Compressor*.bat) do start /wait "" "%%~fX"
)
robocopy "%EU1%\BattlEye" "%Backupmaster%\%EU1TARGET%\BattlEye" /mir /R:2 /W:2 /LOG:"%Backupmaster%\backup.log" /tee /np
robocopy "%EU1%\Keys" "%Backupmaster%\%EU1TARGET%\Keys" /mir /R:2 /W:2 /xf .*  /xd .* /LOG+:"%Backupmaster%\backup.log" /tee /np
robocopy "%EU1%\MPMissions" "%Backupmaster%\%EU1TARGET%\MPMissions" /mir /R:2 /W:2 /xf .* /xd .* /LOG+:"%Backupmaster%\backup.log" /tee /np
robocopy "%EU1%\logs" "%Backupmaster%\%EU1TARGET%\logs" /mir /R:2 /W:2 /LOG+:"%Backupmaster%\backup.log" /tee /np
robocopy "%EU1%\Bonus" "%Backupmaster%\%EU1TARGET%\Bonus" /mir /R:2 /W:2 /LOG+:"%Backupmaster%\backup.log" /tee /np
robocopy "%EU1%\userconfig" "%Backupmaster%\%EU1TARGET%\userconfig" /mir /R:2 /W:2 /LOG+:"%Backupmaster%\backup.log" /tee /np
robocopy "%EU1%" "%Backupmaster%\%EU1TARGET%" *.cfg /mir /R:2 /W:2 /purge /LOG+:"%Backupmaster%\backup.log" /tee /np
robocopy "%EU1%" "%Backupmaster%\%EU1TARGET%" *.bat /mir /R:2 /W:2 /purge /LOG+:"%Backupmaster%\backup.log" /tee /np
robocopy "%EU1%" "%Backupmaster%\%EU1TARGET%" *.exe /mir /R:2 /W:2 /purge /LOG+:"%Backupmaster%\backup.log" /tee /np
robocopy "%EU1%" "%Backupmaster%\%EU1TARGET%" *.config /mir /R:2 /W:2 /purge /LOG+:"%Backupmaster%\backup.log" /tee /np
robocopy "%EU1%" "%Backupmaster%\%EU1TARGET%" *.zip /mir /R:2 /W:2 /purge /LOG+:"%Backupmaster%\backup.log" /tee /np
robocopy "%EU1%" "%Backupmaster%\%EU1TARGET%" *.config /mir /R:2 /W:2 /purge /LOG+:"%Backupmaster%\backup.log" /tee /np
robocopy "%EU1%" "%Backupmaster%\%EU1TARGET%" *.log /mir /R:2 /W:2 /purge /LOG+:"%Backupmaster%\backup.log" /tee /np

cls
for /L %%A in (%INTERVAL%,-1,0) do ( 
echo.
echo =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
echo            AhoyWorld.co.uk - Server Backup by Kamaradski
echo =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
echo.
echo Starting backup "Arma3 EU#2" in %%A seconds.
echo.
ping localhost -n 2 >nul 
cls 
)
if /i %LOGCOMPRESSION% == YES (
	cd "%EU2%"
	for /r %%X in (Compressor*.bat) do start /wait "" "%%~fX"
)
robocopy "%EU2%\BattlEye" "%Backupmaster%\%EU2TARGET%\BattlEye" /mir /R:2 /W:2 /LOG+:"%Backupmaster%\backup.log" /tee /np
robocopy "%EU2%\Keys" "%Backupmaster%\%EU2TARGET%\Keys" /mir /R:2 /W:2 /xf .*  /xd .* /LOG+:"%Backupmaster%\backup.log" /tee /np
robocopy "%EU2%\MPMissions" "%Backupmaster%\%EU2TARGET%\MPMissions" /mir /R:2 /W:2 /xf .* /xd .* /LOG+:"%Backupmaster%\backup.log" /tee /np
robocopy "%EU2%\logs" "%Backupmaster%\%EU2TARGET%\logs" /mir /R:2 /W:2 /LOG+:"%Backupmaster%\backup.log" /tee /np
robocopy "%EU2%\Bonus" "%Backupmaster%\%EU2TARGET%\Bonus" /mir /R:2 /W:2 /LOG+:"%Backupmaster%\backup.log" /tee /np
robocopy "%EU2%\userconfig" "%Backupmaster%\%EU2TARGET%\userconfig" /mir /R:2 /W:2 /LOG+:"%Backupmaster%\backup.log" /tee /np
robocopy "%EU2%" "%Backupmaster%\%EU2TARGET%" *.cfg /mir /R:2 /W:2 /purge /LOG+:"%Backupmaster%\backup.log" /tee /np
robocopy "%EU2%" "%Backupmaster%\%EU2TARGET%" *.bat /mir /R:2 /W:2 /purge /LOG+:"%Backupmaster%\backup.log" /tee /np
robocopy "%EU2%" "%Backupmaster%\%EU2TARGET%" *.exe /mir /R:2 /W:2 /purge /LOG+:"%Backupmaster%\backup.log" /tee /np
robocopy "%EU2%" "%Backupmaster%\%EU2TARGET%" *.config /mir /R:2 /W:2 /purge /LOG+:"%Backupmaster%\backup.log" /tee /np
robocopy "%EU2%" "%Backupmaster%\%EU2TARGET%" *.zip /mir /R:2 /W:2 /purge /LOG+:"%Backupmaster%\backup.log" /tee /np
robocopy "%EU2%" "%Backupmaster%\%EU2TARGET%" *.config /mir /R:2 /W:2 /purge /LOG+:"%Backupmaster%\backup.log" /tee /np
robocopy "%EU2%" "%Backupmaster%\%EU2TARGET%" *.log /mir /R:2 /W:2 /purge /LOG+:"%Backupmaster%\backup.log" /tee /np

cls
for /L %%A in (%INTERVAL%,-1,0) do ( 
echo.
echo =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
echo            AhoyWorld.co.uk - Server Backup by Kamaradski
echo =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
echo.
echo Starting backup "Arma3 EU#3_Gamenight" in %%A seconds.
echo.
ping localhost -n 2 >nul 
cls 
)
if /i %LOGCOMPRESSION% == YES (
	cd "%EU3%"
	for /r %%X in (Compressor*.bat) do start /wait "" "%%~fX"
)
robocopy "%EU3%\BattlEye" "%Backupmaster%\%EU3TARGET%\BattlEye" /mir /R:2 /W:2 /LOG+:"%Backupmaster%\backup.log" /tee /np
robocopy "%EU3%\Keys" "%Backupmaster%\%EU3TARGET%\Keys" /mir /R:2 /W:2 /xf .*  /xd .* /LOG+:"%Backupmaster%\backup.log" /tee /np
robocopy "%EU3%\MPMissions" "%Backupmaster%\%EU3TARGET%\MPMissions" /mir /R:2 /W:2 /xf .* /xd .* /LOG+:"%Backupmaster%\backup.log" /tee /np
robocopy "%EU3%\logs" "%Backupmaster%\%EU3TARGET%\logs" /mir /R:2 /W:2 /LOG+:"%Backupmaster%\backup.log" /tee /np
robocopy "%EU3%\Bonus" "%Backupmaster%\%EU3TARGET%\Bonus" /mir /R:2 /W:2 /LOG+:"%Backupmaster%\backup.log" /tee /np
robocopy "%EU3%\TADST" "%Backupmaster%\%EU3TARGET%\TADST" /mir /R:2 /W:2 /LOG+:"%Backupmaster%\backup.log" /tee /np
robocopy "%EU3%\userconfig" "%Backupmaster%\%EU3TARGET%\userconfig" /mir /R:2 /W:2 /LOG+:"%Backupmaster%\backup.log" /tee /np
robocopy "%EU3%" "%Backupmaster%\%EU3TARGET%" *.cfg /mir /R:2 /W:2 /purge /LOG+:"%Backupmaster%\backup.log" /tee /np
robocopy "%EU3%" "%Backupmaster%\%EU3TARGET%" *.bat /mir /R:2 /W:2 /purge /LOG+:"%Backupmaster%\backup.log" /tee /np
robocopy "%EU3%" "%Backupmaster%\%EU3TARGET%" *.exe /mir /R:2 /W:2 /purge /LOG+:"%Backupmaster%\backup.log" /tee /np
robocopy "%EU3%" "%Backupmaster%\%EU3TARGET%" *.config /mir /R:2 /W:2 /purge /LOG+:"%Backupmaster%\backup.log" /tee /np
robocopy "%EU3%" "%Backupmaster%\%EU3TARGET%" *.zip /mir /R:2 /W:2 /purge /LOG+:"%Backupmaster%\backup.log" /tee /np
robocopy "%EU3%" "%Backupmaster%\%EU3TARGET%" *.config /mir /R:2 /W:2 /purge /LOG+:"%Backupmaster%\backup.log" /tee /np
robocopy "%EU3%" "%Backupmaster%\%EU3TARGET%" *.log /mir /R:2 /W:2 /purge /LOG+:"%Backupmaster%\backup.log" /tee /np


cls
for /L %%A in (%INTERVAL%,-1,0) do ( 
echo.
echo =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
echo            AhoyWorld.co.uk - Server Backup by Kamaradski
echo =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
echo.
echo Starting backup "Game server Tools" in %%A seconds.
echo.
ping localhost -n 2 >nul 
cls 
)

if /i %LOGCOMPRESSION% == YES (
	cd "%TOOL%\Bec"
	for /r %%X in (Compressor*.bat) do start /wait "" "%%~fX"
)
robocopy "%TOOL%" "%Backupmaster%\%TOOLTARGET%" /mir /R:2 /W:2 /xd A3_Master /LOG+:"%Backupmaster%\backup.log" /tee /np

cls
for /L %%A in (%INTERVAL%,-1,0) do ( 
echo.
echo =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
echo            AhoyWorld.co.uk - Server Backup by Kamaradski
echo =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
echo.
echo Starting backup "Installed Apps" in %%A seconds.
echo.
ping localhost -n 2 >nul 
cls 
)

if /i %LOGCOMPRESSION% == YES (
	cd "%APPS%"
	for /r %%X in (Compressor*.bat) do start /wait "" "%%~fX"
)
robocopy "%APPS%" "%Backupmaster%\%APPSTARGET%" /mir /R:2 /W:2 /xd Steam /LOG+:"%Backupmaster%\backup.log" /tee /np

cls
for /L %%A in (%INTERVAL%,-1,0) do ( 
echo.
echo =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
echo            AhoyWorld.co.uk - Server Backup by Kamaradski
echo =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
echo.
echo Starting backup "Minecraft Vanilla" in %%A seconds.
echo.
ping localhost -n 2 >nul 
cls 
)

if /i %LOGCOMPRESSION% == YES (
	cd "%MCV%"
	for /r %%X in (Compressor*.bat) do start /wait "" "%%~fX"
)
robocopy "%MCV%" "%Backupmaster%\%MCVTARGET%" /mir /R:2 /W:2 /xd Exec Modern Public Backups dynmap /LOG+:"%Backupmaster%\backup.log" /tee /np

cls
for /L %%A in (%INTERVAL%,-1,0) do ( 
echo.
echo =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
echo            AhoyWorld.co.uk - Server Backup by Kamaradski
echo =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
echo.
echo Starting backup "Minecraft Modded" in %%A seconds.
echo.
ping localhost -n 2 >nul 
cls 
)

if /i %LOGCOMPRESSION% == YES (
	cd "%MCM%
	for /r %%X in (Compressor*.bat) do start /wait "" "%%~fX"
)
robocopy "%MCM%" "%Backupmaster%\%MCMTARGET%" /mir /R:2 /W:2 /xd Exec Modern Public Backups dynmap /LOG+:"%Backupmaster%\backup.log" /tee /np


cls
exit