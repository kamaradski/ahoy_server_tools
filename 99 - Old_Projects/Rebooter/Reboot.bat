@echo off 
cls

REM -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
REM File:			Reboot.bat
REM Version:		V1.3
REM Author:			Kamaradski 2014
REM Contributers:	none
REM
REM Dedicated server reboot script written for Ahoyworld.co.uk
REM
REM -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-


REM =-=-=-=-=-=-=-=-=-=-=-=-=-=- CONFIGURATION -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

set initialwait=90
set "Start1name=Teamspeak Server"
set "start1file=C:\Games\Tools\A3_Scripts\Teamspeak-Server.lnk"

set wait2=90
set "start2name=A3_Server_EU#1"
set "start2file=C:\Games\Tools\A3_Scripts\A3_Server_EU1.bat"

set wait3=120
set "start3name=A3_Server_EU#2"
set "start3file=C:\Games\Tools\A3_Scripts\A3_Server_EU2.bat"

set wait4=120
set "start4name=Minecraft Vanilla"
set "start4file=C:\Games\Tools\A3_Scripts\MC-Vanilla.lnk"

set wait5=120
set "start5name=Minecraft Modded"
set "start5file=C:\Games\Tools\A3_Scripts\MC-Modded.lnk"


REM =-=-=-=-=-=-=-=-=-=-=-=-=- DO NOT EDIT BELOW HERE -=-=-=-=-=-=-=-=-=-=-=-=-=-




for /L %%A in (%initialwait%,-1,0) do ( 
echo.
echo =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
echo            AhoyWorld.co.uk - Flagship rebooter by Kamaradski
echo =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
echo.
echo Starting "%Start1name%" in %%A seconds.
echo.
ping localhost -n 2 >nul 
cls 
)
start "Starting %Start1name%" %start1file%

for /L %%A in (%wait2%,-1,0) do ( 
echo.
echo =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
echo            AhoyWorld.co.uk - Flagship rebooter by Kamaradski
echo =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
echo.                                 
echo Starting "%start2name%" in %%A seconds.
ping localhost -n 2 >nul 
cls 
)
start "Starting %Start2name%" %start2file%

for /L %%A in (%wait3%,-1,0) do ( 
echo.
echo =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
echo            AhoyWorld.co.uk - Flagship rebooter by Kamaradski
echo =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
echo.                                 
echo Starting "%start3name%" in %%A seconds.
ping localhost -n 2 >nul 
cls 
)
start "Starting %Start3name%" %start3file%

for /L %%A in (%wait4%,-1,0) do ( 
echo.
echo =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
echo            AhoyWorld.co.uk - Flagship rebooter by Kamaradski
echo =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
echo.                                 
echo Starting "%start4name%" in %%A seconds.
ping localhost -n 2 >nul 
cls 
)
start "Starting %Start4name%" %start4file%

for /L %%A in (%wait5%,-1,0) do ( 
echo.
echo =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
echo            AhoyWorld.co.uk - Flagship rebooter by Kamaradski
echo =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
echo.                                 
echo Starting "%start5name%" in %%A seconds.
ping localhost -n 2 >nul 
cls 
)
start "Starting %Start5name%" %start5file%


for /L %%A in (%initialwait%,-1,0) do ( 
echo.
echo =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
echo            AhoyWorld.co.uk - Flagship rebooter by Kamaradski
echo =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
echo.                                 
echo Server is rebooted, services are back up and running, enjoy...
ping localhost -n 2 >nul 
cls 
)
rem.