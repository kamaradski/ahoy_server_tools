// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// File:			A3_Restarter.ps1
// Author:			Kamaradski
//
// //Ahoyworld.co.uk - Arma3 restarter by Kamaradski
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-


What:
This script will start & re-start your Arma3 server with BEC and performs maintenance


Documentation:
https://docs.google.com/document/d/1tjpWVrarHHUY2zxQe-1HFdEyeus_aDumcN-sUYXBnmA


Main features:
- Start & re-start Arma3 Server
- Start & re-start BEC
- Update and validate your Arma3 Server installation via SteamCMD
- Sync Keys, Missions and BE-configurations across servers
- Clean and archive log-files (Server & BEC)
- Automatic Server Configuration & Log backup (incl BEC)
- Supports TADST profiles


Usage Licence & Warranty disclaimer:
See Documentation - page-2


Contribution:
In case you like to improve this script: Send us your code suggestions on bitbucket or email


Donations:
If you like this script, please support us by donating something to our paypal:
https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=WHH8VTC58XU7G