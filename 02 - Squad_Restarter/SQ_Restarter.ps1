# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
# File:			SQ_Restarter.ps1
# Version:		V1.0
# Author:		Kamaradski 2015
# Contributers:	None
#
# Squad Server (re)starter by Ahoyworld.co.uk
# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

Write-Host ""
Write-Host -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
Write-Host "             //" -ForegroundColor Red -NoNewline;
Write-Host "AhoyWorld.co.uk" -NoNewline;
Write-Host " - Squad restarter by Kamaradski"
Write-Host -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
Write-Host ""
Write-Host ""
Write-Host "Initizing..."

# Parsing config-file:
$p = $MyInvocation.MyCommand.Definition; $p = $p.Substring(0,$p.Length-4); $r = $p + ".cfg"
$q = $MyInvocation.MyCommand.Definition
Get-Content "$r" | foreach-object -begin {$h=@{}} -process { $k = [regex]::split($_,' = '); if(($k[0].CompareTo("") -ne 0) -and ($k[0].StartsWith("[") -ne $True)) { $h.Add($k[0], $k[1]) } }

#displayname:
$Script:ServerName=$h.Get_Item("ServerName")

#backupopions:
$Script:EnableBackup=$h.Get_Item("EnableBackup")
$Script:PathBackupDestination=$h.Get_Item("PathBackupDestination")

#steamupdater:
$Script:EnableUpdateOnRestart=$h.Get_Item("EnableUpdateOnRestart")
$Script:SteamLogin=$h.Get_Item("SteamLogin")
$Script:SteamPassword=$h.Get_Item("SteamPassword")
$Script:PathSteamCMD=$h.Get_Item("PathSteamCMD")
$Script:SteamAppNumber=$h.Get_Item("SteamAppNumber")

#serversettings:
$Script:AssignCPUCore=$h.Get_Item("AssignCPUCore")
$Script:ServerCPUAffinity=$h.Get_Item("ServerCPUAffinity")
$Script:ServerPath=$h.Get_Item("ServerPath")
$Script:ServerIP=$h.Get_Item("ServerIP")
$Script:ServerPort=$h.Get_Item("ServerPort")
$Script:SteamQueryPort=$h.Get_Item("SteamQueryPort")
$Script:ServerMapRandom=$h.Get_Item("ServerMapRandom")
$Script:ServerParameters=$h.Get_Item("ServerParameters")


# Setting variables:
$p = $MyInvocation.MyCommand.Definition; $p = $p.Substring(0,$p.Length-4); $p = $p + ".log"
$Script:IntervalShort = 1
$Script:IntervalMedium = 5
$Script:IntervalLong = 20
$Script:PathRestarterLogFile = "$p"
$Script:FullPathSquadServerExe = "$($ServerPath)\SquadServer.exe"
$Script:FullPathTempLog = "$($ServerPath)\temp.log"
$Script:gamer = 'happy'

$host.ui.RawUI.WindowTitle = "Restarter: $ServerName - port: $ServerPort"


# Update Squad server via SteamCMD
Function RunSteamUpdate {
	$a=(Get-Date).ToUniversalTime()
	$b = "$a  -  Checking Steam for updates... "; Add-Content $PathRestarterLogFile $b
	Write-Host $a.ToShortTimeString() " -  Checking Steam for updates..."
	Start-Process $PathSteamCMD\steamcmd.exe "+login $SteamLogin $SteamPassword +force_install_dir $ServerPath +app_update $SteamAppNumber -validate +quit" -NoNewWindow -Wait -RedirectStandardOutput $FullPathTempLog
	add-content $PathRestarterLogFile -value (get-content $FullPathTempLog)
	del $FullPathTempLog
}


# Starting the server
Function RUNStartSquadServer {
	$a=(Get-Date).ToUniversalTime()
	$b = "$a  -  Starting: $ServerName on port: $ServerPort "; Add-Content $PathRestarterLogFile $b
	Write-Host $a.ToShortTimeString() " -  Starting: $ServerName on port: $ServerPort"
	
	$Script:SquadServerID = Start-Process SquadServer.exe "MULTIHOME=$ServerIP Port=$ServerPort QueryPort=$SteamQueryPort RANDOM=$ServerMapRandom $ServerParameters" -WorkingDirectory $ServerPath -passthru
	Start-Sleep -s $IntervalLong
	
	$CheckSquadServerRunning = Get-Process SquadServer -ErrorAction SilentlyContinue | Where-Object {$_.Path -like "$ServerPath*"}
	$RunningPIDs=@()
	Foreach ($i in $CheckSquadServerRunning) {
		$RunningPIDs += $($i.Id)
		$a=(Get-Date).ToUniversalTime()
		$b = "$a  -  $ServerName started with PID: $($i.Id) "; Add-Content $PathRestarterLogFile $b
		Write-Host $a.ToShortTimeString() " -  $ServerName started with PID: $($i.Id)" -ForegroundColor "Green"
	}
	
	Start-Sleep -s $IntervalShort
	Write-Host ''
	
	if ($AssignCPUCore -eq "YES") {
		$SetAffinity = Get-Process SquadServer -ErrorAction SilentlyContinue | Where-Object {$_.Path -like "$ServerPath*"}
		Foreach ($i in $RunningPIDs) {
			$SetAffinity = Get-Process SquadServer -ErrorAction SilentlyContinue | Where-Object {$_.Id -eq $i}
			$SetAffinity.ProcessorAffinity=[int]$ServerCPUAffinity
			$a=(Get-Date).ToUniversalTime()
			$b = "$a  -  PID: $i assigned to CPU-core(s): $ServerCPUAffinity"; Add-Content $PathRestarterLogFile $b
			Write-Host $a.ToShortTimeString() " -  PID: $i assigned to CPU-core(s): $ServerCPUAffinity"
		}
	}
}



# Check if Squad Server is still running
Function IsServerRunning {
		$Script:CheckSquadServerRunning  = Get-Process SquadServer -ErrorAction SilentlyContinue | Where-Object {$_.Id -eq $($SquadServerID.Id) } | Where-Object {$_.Responding -eq $true}
}



# Kill Squad Server process
Function KILLSERVER {
	$CheckSquadServerRunning = Get-Process SquadServer -ErrorAction SilentlyContinue | Where-Object {$_.Path -like "$ServerPath*"}
	if ($CheckSquadServerRunning) {
		Foreach ($i in $CheckSquadServerRunning) {
			Stop-Process $($i.Id)
			$a=(Get-Date).ToUniversalTime()
			Write-Host $a.ToShortTimeString() " -  APP:SquadServer.exe PID:$($i.Id) killed"
			$b = "$a  -  APP:SquadServer.exe PID:$($i.Id) killed "; Add-Content $PathRestarterLogFile $b
		}
	}
}


# Start-up sequence
Function StartSequence {
	if ($EnableBackup  -eq "YES") {
		RUNServerBackup
	}
	if ($EnableUpdateOnRestart  -eq "YES") {
		RunSteamUpdate
	}
	Write-Host ''
	$b = ''; Add-Content $PathRestarterLogFile $b
	RUNStartSquadServer
	Start-Sleep -s $IntervalLong
	Start-Sleep -s $IntervalLong
}



# Make a Backup of the config for this server (and BEC), and it's log-history
Function RUNServerBackup {

	# Sanitychecks and folder creation
	$a=(Get-Date).ToUniversalTime()
	$b = "$a  -  Backing up Server configuration... "; Add-Content $PathRestarterLogFile $b
	Write-Host $a.ToShortTimeString() " -  Backing up Server configuration...  "
	if (Test-Path $PathBackupDestination\$ServerName ) {
		$a=(Get-Date).ToUniversalTime()
		$b = "$a  -  Previous Backup found: $PathBackupDestination\$ServerName "; Add-Content $PathRestarterLogFile $b
	}Else{
		New-Item $PathBackupDestination\$ServerName -type directory | Out-Null
		$a=(Get-Date).ToUniversalTime()
		$b = "$a  -  Backup folder created: $PathBackupDestination\$ServerName "; Add-Content $PathRestarterLogFile $b
		Write-Host $a.ToShortTimeString() " -  Backup folder created: $PathBackupDestination\$ServerName " -ForegroundColor "Yellow"
	}
	
	# Backup server
	Start-Process robocopy "$ServerPath $PathBackupDestination\$ServerName /mir " -NoNewWindow -Wait -RedirectStandardOutput $FullPathTempLog		
	
	# Backup the restarter
	if (!(Test-Path $PathBackupDestination\$ServerName\Restarter)) {
		New-Item $PathBackupDestination\$ServerName\Restarter -type directory | Out-Null
	}
	
	Copy-Item $PathRestarterLogFile $PathBackupDestination\$ServerName\Restarter
	Copy-Item $r $PathBackupDestination\$ServerName\Restarter
	Copy-Item $q $PathBackupDestination\$ServerName\Restarter

	# Cleanup
	if (!(Test-Path $PathBackupDestination\$ServerName\temp.log)) {
		Remove-Item $PathBackupDestination\$ServerName\temp.log | Out-Null
	}
	del $FullPathTempLog
	Start-Sleep -s $IntervalShort
}

$a=(Get-Date).ToUniversalTime()
Write-Host $a.ToShortTimeString() " -  Running sanity checks ..."

# Sanity check: checking for LogFile & Adding start-header
if (Test-Path $PathRestarterLogFile) {
	$a=(Get-Date).ToUniversalTime()
	$b = "$a ";Set-Content $PathRestarterLogFile $b
	$b = "$a -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-";Add-Content $PathRestarterLogFile $b
	$b = "$a                AhoyWorld.co.uk Squad restarter by Kamaradski";Add-Content $PathRestarterLogFile $b
	$b = "$a -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-";Add-Content $PathRestarterLogFile $b
	$b = "$a ";Add-Content $PathRestarterLogFile $b
	$b = "$a ";Add-Content $PathRestarterLogFile $b
	$b = "$a  -  Existing logfile found: $PathRestarterLogFile "; Add-Content $PathRestarterLogFile $b
}Else{
	New-Item $PathRestarterLogFile -type file | Out-Null
	$a=(Get-Date).ToUniversalTime()
	$b = "$a ";Add-Content $PathRestarterLogFile $b
	$b = "$a -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-";Add-Content $PathRestarterLogFile $b
	$b = "$a                AhoyWorld.co.uk Squad restarter by Kamaradski";Add-Content $PathRestarterLogFile $b
	$b = "$a -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-";Add-Content $PathRestarterLogFile $b
	$b = "$a ";Add-Content $PathRestarterLogFile $b
	$b = "$a ";Add-Content $PathRestarterLogFile $b
	$b = "$a  -  Created new Logfile $PathRestarterLogFile "; Add-Content $PathRestarterLogFile $b
	Write-Host $a.ToShortTimeString() " -  Created new Logfile $PathRestarterLogFile" -ForegroundColor "Yellow"
}
Start-Sleep -s $IntervalShort



$a=(Get-Date).ToUniversalTime()
$b = "$a  -  Info read from config-file"; Add-Content $PathRestarterLogFile $b
$b = "$a  -  Config file found: $p"; Add-Content $PathRestarterLogFile $b
$b = "$a  -  Config parameters read:"; Add-Content $PathRestarterLogFile $b
$h.GetEnumerator() | Sort-Object Name | ForEach-Object {"{0} : {1}" -f $_.Name,$_.Value} | Add-Content $PathRestarterLogFile




# Sanity check: checking for running SquadServer duplicates (same folder) & Kill if detected
$Script:CheckSquadServerRunning = Get-Process SquadServer -ErrorAction SilentlyContinue | Where-Object {$_.Path -like "$ServerPath*"}
if ($CheckSquadServerRunning) {
	Foreach ($i in $CheckSquadServerRunning) {
		Stop-Process $($i.Id)
		$a=(Get-Date).ToUniversalTime()
		Write-Host $a.ToShortTimeString() " -  Old Squad-server detected: killed PID:$($i.Id)"
		$b = "$a  -  Old Squad-server detected: killed PID:$($i.Id) "; Add-Content $PathRestarterLogFile $b
	}
}
Start-Sleep -s $IntervalShort



# Sanity check: checking for SquadServer executable
if (Test-Path $FullPathSquadServerExe) {
	$a=(Get-Date).ToUniversalTime()
	$b = "$a  -  Squad Server found: $FullPathSquadServerExe "; Add-Content $PathRestarterLogFile $b
}Else{
	$a=(Get-Date).ToUniversalTime()
	$b = "$a  -  Squad Server NOT found: $FullPathSquadServerExe "; Add-Content $PathRestarterLogFile $b
	$b = "$a  -  Exiting script ... "; Add-Content $PathRestarterLogFile $b
	Write-Host $a.ToShortTimeString() " -  Squad Server NOT found: $FullPathSquadServerExe" -BackgroundColor "Red" -ForegroundColor "white"
	Write-Host $a.ToShortTimeString() " -  Exiting script ..." -BackgroundColor "Red" -ForegroundColor "white"
	Start-Sleep -s $IntervalLong
	exit
}
Start-Sleep -s $IntervalShort



# Sanity check: checking Backup Destination
if ($EnableBackup -eq "YES") {
	if (Test-Path $PathBackupDestination) {
		$a=(Get-Date).ToUniversalTime()
		$b = "$a  -  Backup folder found: $PathBackupDestination "; Add-Content $PathRestarterLogFile $b
	}Else{
		$a=(Get-Date).ToUniversalTime()
		$b = "$a  -  Backup folder NOT found: $PathBackupDestination "; Add-Content $PathRestarterLogFile $b
		$b = "$a  -  Exiting script ... "; Add-Content $PathRestarterLogFile $b
		Write-Host $a.ToShortTimeString() " -  Backup folder NOT found: $PathBackupDestination" -BackgroundColor "Red" -ForegroundColor "white"
		Write-Host $a.ToShortTimeString() " -  Exiting script ..." -BackgroundColor "Red" -ForegroundColor "white"
		Start-Sleep -s $IntervalLong
		exit
	}
}
Start-Sleep -s $IntervalShort




# Initial startup
StartSequence




# Main loop
Do {
	# check if Squad server is running
	IsServerRunning
	if (!$CheckSquadServerRunning) {
		$a=(Get-Date).ToUniversalTime()
		$b = ''; Add-Content $PathRestarterLogFile $b
		$b = "$a  -  APP:SquadServer.exe PID:$($SquadServerID.Id) crashed: restarting ... "; Add-Content $PathRestarterLogFile $b
		Write-Host ''
		Write-Host $a.ToShortTimeString() " -  APP:SquadServer.exe PID:$($SquadServerID.Id) crashed: restarting ..." -BackgroundColor "Red" -ForegroundColor "white"
		Start-Sleep -s $IntervalMedium
		KILLSERVER
		Start-Sleep -s $IntervalMedium
		StartSequence
	}
	Start-Sleep -s $IntervalLong
} While ($gamer –eq ‘happy’)